package iOS.TestDefinitions;

import base.AppDriver;
import base.Utils;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

public class LoginFlow {


    Utils u = new Utils();

    public LoginFlow(){

        PageFactory.initElements(new AppiumFieldDecorator(AppDriver.getDriver()), this);
    }


   // @FindBy(name = "CONTINUE")
  //  public WebElement continueBtn;

     By continueBtn = By.name("CONTINUE");
    By email = By.name("EMAIL_ADDRESS_TEXTFIELD");
    By description = By.name("DESCRIPTION");
    By password = By.name("PASSWORD_TEXTFIELD");
    By header = By.name("HEADER");
    By profile = By.name("ic profile");
    By accountDetails = By.name("Account Details");

    By backToProfile = By.name("ic arrow left");
    By logout = By.name("LOGOUT");



   // By by_createNewContact = By.xpath("//android.widget.TextView[@text='Create new contact']");
 //   By by_FirstName = By.xpath("//android.widget.EditText[@text='First name']");

    public String loginToLogout(String emailVal, String passwordVal) throws Exception{

           String getEmailAddress = null;

        u.implicitWait();
        u.clickElement(continueBtn, "Continue");


        try {
            u.enterVal(email,"Email Address", emailVal);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("FAILED ::"+e.getMessage());
        }


        u.clickElement(description,"Tap on Description");
        u.clickElement(continueBtn, "Continue");

        try {
            u.enterVal(password,"Password", passwordVal);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("FAILED ::"+e.getMessage());
        }


        u.clickElement(header,"Tap on Header");
        u.clickElement(continueBtn, "Continue");
        u.clickElement(profile, "Profile button");
        u.clickElement(accountDetails, "Account Details");



            try {
                getEmailAddress =  AppDriver.getDriver().findElement(By.name("purwar389@gmail.com")).getText();
            }catch (Exception e){
                e.printStackTrace();
                System.out.println("Failed! "+ e.getMessage());
                getEmailAddress ="";//set blank to assert if no element found
            }

        u.clickElement(backToProfile, "Back to Profile");
        u.clickElement(logout, "LOGOUT");


        return getEmailAddress;

        }

        public void logout(){
            u.clickElement(profile, "Profile button");
            u.clickElement(logout, "LOGOUT");

        }

    public String enterInvalidEmailDuringLogin(String emailVal) {

        u.implicitWait();
        u.clickElement(continueBtn, "Continue");
        u.enterVal(email,"Email Address", emailVal);
        u.clickElement(description,"Tap on Description");
        String  errorMsg =  AppDriver.getDriver().findElement(By.name(" Please enter a valid email address ")).getText().trim();
        return errorMsg;
    }

    public String enterInvalidPasswordDuringLogin(String emailVal, String passwordVal) {

        u.implicitWait();
        u.clickElement(continueBtn, "Continue");
        u.enterVal(email,"Email Address", emailVal);
        u.clickElement(description,"Tap on Description");
        u.clickElement(continueBtn, "Continue");
        u.enterVal(password,"Password", passwordVal);
        u.clickElement(header,"Tap on Header");
        u.clickElement(continueBtn, "Continue");
        String  errorMsg =  AppDriver.getDriver().findElement(By.name("Your password is invalid or the email address does not have an account. Please try again")).getText().trim();
        return errorMsg;
    }
}

