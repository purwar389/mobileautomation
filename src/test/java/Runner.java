import Android.ABCiview.Watchlist;
import base.AppFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Properties;

/**
 * @author gauravpurwar
 * Platform All
 * Various test cases for login journey
 * 4 test cases
 */
public class Runner {

    Watchlist iP = new Watchlist();
     Properties prop = new Properties();
     String file = "project.properties";



    @BeforeMethod
    public void Setup() throws MalformedURLException {

        try {
            prop.load(new FileInputStream(file));
        } catch (
                IOException e) {
            e.printStackTrace();
        }

        AppFactory.launchApp(prop.getProperty("platform"));
    }

    /**
     *    Providing different credentials to validate positive and negative test cases - data driven
     */
    @DataProvider (name = "data-provider")
    public Object[][] dpMethod(){
        return new Object[][] {{"Lion","mobileabc389@gmail.com", "Gaurav1!"}, {"lio","purwar389@gmail.com","Gaurav1!"},};
    }

    /**
     *
     * @param emailVal
     * @param passwordVal
     * @throws Exception
     */
    @Test (dataProvider = "data-provider", priority = 1)
    public void validateWatchList (String showName, String emailVal, String passwordVal) throws Exception {

        System.out.println("emailVal: "+emailVal+" passwordVal: "+passwordVal+ " AND Test Passed!");

            String actualShowName = iP.checkWatchlist(showName,emailVal,passwordVal);
            Assert.assertEquals(actualShowName, showName, "Show Name didn't match!");
            System.out.println("Test Passed!");;


    }






    @AfterMethod
    public void tearDown(){
        AppFactory.closeApp();
    }



}
