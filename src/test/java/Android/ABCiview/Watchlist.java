package Android.ABCiview;

import base.AppDriver;
import base.Utils;
import io.appium.java_client.AppiumBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.remote.SupportsContextSwitching;


import java.time.Duration;
import java.util.Set;

public class Watchlist {

    Utils u = new Utils();

    public Watchlist(){

        PageFactory.initElements(new AppiumFieldDecorator(AppDriver.getDriver()), this);
    }

    By loginToWatch = By.id("au.net.abc.iview:id/buttonGetStarted");
    By email = By.id("cockatoo");
    By emailErrAlert = By.id("au.com.podcastoneaustralia:id/textinput_error");
    By description = By.id("au.com.podcastoneaustralia:id/titleTextView");
    By password = By.id("au.com.podcastoneaustralia:id/passwordForLogin");
    By passwordErrAlert = By.id("au.com.podcastoneaustralia:id/errorAlertTextView");
    // By header = By.name("HEADER");
    By profile = By.id("au.com.podcastoneaustralia:id/profileImage");
    By accountDetails = By.xpath("//androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.TextView");
    By emailTextVerify = By.id("au.com.podcastoneaustralia:id/emailTextView");
    By backToProfile = By.className("android.widget.ImageView");
    By logout = By.id("au.com.podcastoneaustralia:id/buttonTitleTextView");
    By continueBtn;

    public String checkWatchlist(String showName, String emailVal, String passwordVal) throws Exception{


        u.implicitWait();
        u.clickElement(By.id("au.net.abc.iview:id/buttonGetStarted"),"Get Started!");
        u.implicitWait();

        u.implicitWait();
        Set<String> contextNames = ((SupportsContextSwitching) AppDriver.getDriver()).getContextHandles();
        for (String contextName : contextNames) {
            System.out.println("Print Context Name: "+contextName); //prints out something like NATIVE_APP \n WEBVIEW_1
        }

        ((SupportsContextSwitching) AppDriver.getDriver()).context((String) contextNames.toArray()[1]);
        u.implicitWait(60);
        u.clickElement(By.xpath("//button[text()='Log In With Email']"),"Login with Email!");
        u.enterVal(By.name("email"),"Email", emailVal);
        u.enterVal(By.name("password"),"Password", passwordVal);
        u.clickElement(By.xpath("//button[text()='Log In With Email']"),"Login with Email and Password!");

        u.implicitWait(60);
        ((SupportsContextSwitching) AppDriver.getDriver()).context((String) contextNames.toArray()[0]);
        u.clickElement(AppiumBy.ByAccessibilityId.accessibilityId("cockatoo"),"Click Gaurav");
        u.clickElement(AppiumBy.ByAccessibilityId.accessibilityId("My account"),"Click Profile");
        u.clickElement(By.xpath("//android.widget.TextView[@text='Logout of iview']"),"Logout of iview");

        u.implicitWait(100);



        AppDriver.getDriver().findElement(By.id("cockatoo")).click();
        AppDriver.getDriver().findElement(By.id("Open navigation drawer")).click();
        AppDriver.getDriver().findElement(By.id("au.net.abc.iview:id/layout_nav_drawer_parent_indicator")).click();
        AppDriver.getDriver().findElement(By.id("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[5]/android.widget.TextView")).click();
        AppDriver.getDriver().findElement(By.id("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[5]/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[2]/android.widget.TextView")).click();
        AppDriver.getDriver().findElement(By.id("//android.view.ViewGroup[@content-desc=\"MOVIE,Lion,duration 1 hour 53 minutes\"]/android.view.ViewGroup/android.view.ViewGroup/android.widget.ImageView")).click();
        AppDriver.getDriver().findElement(By.id("au.net.abc.iview:id/program_info_watchlist_button")).click();
        AppDriver.getDriver().findElement(By.id("My account")).click();
        AppDriver.getDriver().findElement(By.id("au.net.abc.iview:id/menu_logout")).click();


        u.implicitWait();

        return "true";

    }

    public void logout(){
        u.clickElement(profile, "Profile button");
        u.clickElement(logout, "LOGOUT");

    }

    public String enterInvalidEmailDuringLogin(String emailVal) {

        u.implicitWait();

        u.clickElement(continueBtn, "Continue");
        u.enterVal(email,"Email Address", emailVal);
        u.clickElement(description,"Tap on Description");
        String  errorMsg =  AppDriver.getDriver().findElement(emailErrAlert).getText().trim();
        return errorMsg;
    }

    public String enterInvalidPasswordDuringLogin(String emailVal, String passwordVal) {

        u.implicitWait();
        u.clickElement(continueBtn, "Continue");
        u.enterVal(email,"Email Address", emailVal);
        u.clickElement(description,"Tap out");
        u.clickElement(continueBtn, "Continue");
        u.enterVal(password,"Password", passwordVal);
        u.clickElement(description,"Tap out");
        u.clickElement(continueBtn, "Continue");
        String  errorMsg =  AppDriver.getDriver().findElement(passwordErrAlert).getText().trim();
        return errorMsg;
    }



}