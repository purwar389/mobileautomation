package Android.TestDefinitions;

import base.AppDriver;
import base.Utils;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

public class LoginFlow {

    Utils u = new Utils();

    public LoginFlow(){

        PageFactory.initElements(new AppiumFieldDecorator(AppDriver.getDriver()), this);
    }

    By continueBtn = By.id("au.com.podcastoneaustralia:id/toolBarButton");
    By email = By.id("au.com.podcastoneaustralia:id/emailEditText");
    By emailErrAlert = By.id("au.com.podcastoneaustralia:id/textinput_error");
    By description = By.id("au.com.podcastoneaustralia:id/titleTextView");
    By password = By.id("au.com.podcastoneaustralia:id/passwordForLogin");
    By passwordErrAlert = By.id("au.com.podcastoneaustralia:id/errorAlertTextView");
   // By header = By.name("HEADER");
    By profile = By.id("au.com.podcastoneaustralia:id/profileImage");
    By accountDetails = By.xpath("//androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.TextView");
    By emailTextVerify = By.id("au.com.podcastoneaustralia:id/emailTextView");
    By backToProfile = By.className("android.widget.ImageView");
    By logout = By.id("au.com.podcastoneaustralia:id/buttonTitleTextView");

    public String loginToLogout(String emailVal, String passwordVal) throws Exception{

        String getEmailAddress = null;

        u.implicitWait();
        u.clickElement(continueBtn, "Continue");


        try {
            u.enterVal(email,"Email Address", emailVal);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("FAILED ::"+e.getMessage());
        }


        u.clickElement(description,"Tap out");
        u.clickElement(continueBtn, "Continue");

        try {
            u.enterVal(password,"Password", passwordVal);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("FAILED ::"+e.getMessage());
        }


        u.clickElement(description,"Tap out");
        u.clickElement(continueBtn, "Continue");
        u.clickElement(profile, "Profile button");
        u.clickElement(accountDetails, "Account Details");



        try {
            getEmailAddress =  AppDriver.getDriver().findElement(emailTextVerify).getText();
        }catch (Exception e){
            e.printStackTrace();
            System.out.println("Failed! "+ e.getMessage());
            getEmailAddress ="";//set blank to assert if no element found
        }

        u.clickElement(backToProfile, "Back to Profile");
        u.clickElement(logout, "LOGOUT");


        return getEmailAddress;

    }

    public void logout(){
        u.clickElement(profile, "Profile button");
        u.clickElement(logout, "LOGOUT");

    }

    public String enterInvalidEmailDuringLogin(String emailVal) {

        u.implicitWait();
        u.clickElement(continueBtn, "Continue");
        u.enterVal(email,"Email Address", emailVal);
        u.clickElement(description,"Tap on Description");
        String  errorMsg =  AppDriver.getDriver().findElement(emailErrAlert).getText().trim();
        return errorMsg;
    }

    public String enterInvalidPasswordDuringLogin(String emailVal, String passwordVal) {

        u.implicitWait();
        u.clickElement(continueBtn, "Continue");
        u.enterVal(email,"Email Address", emailVal);
        u.clickElement(description,"Tap out");
        u.clickElement(continueBtn, "Continue");
        u.enterVal(password,"Password", passwordVal);
        u.clickElement(description,"Tap out");
        u.clickElement(continueBtn, "Continue");
        String  errorMsg =  AppDriver.getDriver().findElement(passwordErrAlert).getText().trim();
        return errorMsg;
    }



}