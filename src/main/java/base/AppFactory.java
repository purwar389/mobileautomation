package base;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.AutomationName;
import io.appium.java_client.remote.IOSMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;

/**
 * @author gauravpurwar
 * @date 27 November 2022
 * Class is to launch iOS and Android Apps
 */
public class AppFactory {

    public static AppiumDriver driver;
    public static DesiredCapabilities caps;
    static Properties prop = new Properties();
    public static void Android_LaunchApp() throws MalformedURLException {

        try {
            prop.load(new FileInputStream("android.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }



        caps = new DesiredCapabilities();
        caps.setCapability(MobileCapabilityType.PLATFORM_NAME, prop.getProperty(MobileCapabilityType.PLATFORM_NAME));
        caps.setCapability(MobileCapabilityType.DEVICE_NAME, prop.getProperty(MobileCapabilityType.DEVICE_NAME));
        caps.setCapability(MobileCapabilityType.APP, prop.getProperty(MobileCapabilityType.APP));
        caps.setCapability("appium:ensureWebviewsHavePages", true);
        caps.setCapability("appium:nativeWebScreenshot", true);
        caps.setCapability("appium:newCommandTimeout", 3600);
        caps.setCapability("appium:connectHardwareKeyboard", true);
      //  caps.setCapability("appPackage", "");
      //  caps.setCapability("appActivity", ".MainActivity");

        URL remoteUrl = new URL("http://127.0.0.1:4723/wd/hub");
     //   URL remoteUrl = new URL("http://127.0.0.1:4723");



        driver = new AndroidDriver(remoteUrl, caps);
     //   driver = new AppiumDriver(remoteUrl, caps);

        System.out.println("Launched successfully!");
        AppDriver.setDriver(driver);
        System.out.println("Android Driver is set!");
    }

    public static void iOS_LaunchApp() throws MalformedURLException {


        try {
            prop.load(new FileInputStream("iOS.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        caps = new DesiredCapabilities();
        caps.setCapability(MobileCapabilityType.PLATFORM_NAME,prop.getProperty(MobileCapabilityType.PLATFORM_NAME));
        caps.setCapability(MobileCapabilityType.DEVICE_NAME,prop.getProperty(MobileCapabilityType.DEVICE_NAME));
        caps.setCapability(MobileCapabilityType.APP,prop.getProperty(MobileCapabilityType.APP));
        caps.setCapability(MobileCapabilityType.AUTOMATION_NAME, AutomationName.IOS_XCUI_TEST);
        caps.setCapability(MobileCapabilityType.PLATFORM_VERSION,prop.getProperty(MobileCapabilityType.PLATFORM_VERSION));
        caps.setCapability(MobileCapabilityType.UDID,prop.getProperty(MobileCapabilityType.UDID));
        caps.setCapability(IOSMobileCapabilityType.WDA_LAUNCH_TIMEOUT, 50000);
        caps.setCapability("commandTimeouts", "12000");//.app
        caps.setCapability("xcodeOrgId", "purwar389@gmail.com");
        caps.setCapability("xcodeSigningId","iPhone Developer");
        caps.setCapability("autoGrantPermissions", true);
        caps.setCapability("autoAcceptAlerts", true);


        URL remoteUrl = new URL("http://127.0.0.1:4723/wd/hub");

        driver = new IOSDriver(remoteUrl,caps);
        System.out.println("Launched successfully!");
        AppDriver.setDriver(driver);
        System.out.println("iOS driver is set");

    }


    public static void launchApp(String platform) throws MalformedURLException {

        if(platform.equalsIgnoreCase("iOS")){
            iOS_LaunchApp();
        }
        else if(platform.equalsIgnoreCase("Android")){
            Android_LaunchApp();
        }

    }
    public static void closeApp(){
        driver.quit();
    }

}