package base;


import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.sql.Time;
import java.time.Duration;
import java.util.concurrent.TimeUnit;

public class Utils {


    public static void waitForEl(By locator){
        new WebDriverWait(AppDriver.getDriver(), Duration.ofSeconds(60)).until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    public static void waitForEl(WebElement ele){
        new WebDriverWait(AppDriver.getDriver(), Duration.ofSeconds(60)).until(ExpectedConditions.visibilityOf(ele));
    }


    public void clickElement(By locator, String val) {
        waitForEl(locator);
        AppDriver.getDriver().findElement(locator).click();
        System.out.println(val+" button is clicked!");
    }

    public void enterVal(By locator, String fieldName, String val) {
        waitForEl(locator);
        AppDriver.getDriver().findElement(locator).clear();
        AppDriver.getDriver().findElement(locator).sendKeys(val);
        System.out.println(fieldName+" Field is entered with value "+val+ "");
    }

    public void clickElement(WebElement ele, String val) {
        waitForEl(ele);
        ele.click();
        System.out.println(ele+" button is clicked!");
    }

    public void enterVal(WebElement ele, String fieldName, String val) {
        waitForEl(ele);
        ele.sendKeys(val);
        System.out.println(fieldName+" Field is entered with value "+val+ "");
    }

    public void implicitWait(){
        AppDriver.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(15));
    }

    public void implicitWait(Integer time){
        AppDriver.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(time));
    }

}